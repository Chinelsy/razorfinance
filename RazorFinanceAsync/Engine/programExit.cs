﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RazorFinanceAsync.Engine
{
    class programExit
    {
        public static void ExitApp()
        {
            Console.Clear();
            Console.WriteLine("\n\n\tPress any key to exit...");
            Console.ReadKey();
            Environment.Exit(1);
        }
    }
}
