﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RazorFinanceAsync.Engine
{
    class MainMenu
    {
        public static string[] displayMenu()
        {
            String email = "";
            String password = "";
            string[] valueOfDetails;
            Console.Clear();
            Console.WriteLine("--------------------------------------------");
            Console.WriteLine("Welcome to razorFinance.\n");
            Console.WriteLine("Please enter your details to Proceed\n");
            Console.WriteLine("Enter your email");
            email = Console.ReadLine();
            Console.WriteLine("Enter your password");
            password = Console.ReadLine();
            String details = email + " " +password;
            valueOfDetails = details.Split(" ");
            return valueOfDetails;
        }
    }
}
