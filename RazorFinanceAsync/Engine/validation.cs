﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RazorFinanceAsync.Engine
{
    class validation
    {
        //checks for validity of user Details in the Database(userDetails class) if Valid, displays a welcome msg 
        //and also prompts the user to enter a 1 to view his full credentials of 0 to Logout.
        // I tried to implement these asynchronously... 'not sure face'
        public async Task validateUser()
        {
                
                bool isRunning = true;
                while (isRunning == true)
                {

                    string[] MenuDetails = MainMenu.displayMenu();
                    if ((userDatails.loginCredentials[0]).Equals(MenuDetails[0]) && (userDatails.loginCredentials[1]).Equals(MenuDetails[1]))
                    {
                        isRunning = false;
                    await Task.Delay(100);
                    Console.WriteLine($"Hello {userDatails.userCredentials[0] + " " + userDatails.userCredentials[1] + " Welcome"}");
                        Console.WriteLine("press 1 to view your details or 0 to Logout");
                        string input = Console.ReadLine();
                    await Task.Delay(1000);
                    userInput(input);
                    }
                    else
                    {
                        Console.WriteLine("Invalid details entered please press Enter to input your valid details or 0 to exit ");
                        string input = Console.ReadLine();
                        userInput(input);
                    }


                }
          
        }

        public static void userInput(string optionSelected)
        {
            switch (optionSelected)
            {
                case "0":
                    programExit.ExitApp();
                    break;
                case "1":
                    Console.WriteLine("\t1 firstName : {0}\n \t2 lastName : {1}\n\t3 email : {2} \n\t4 accountNumber: {3} \n\t5 Address: {4} \n ", 
                        userDatails.userCredentials[0], userDatails.userCredentials[1], 
                        userDatails.userCredentials[2], userDatails.userCredentials[3], userDatails.userCredentials[4]);
                    Console.ReadLine();
                    programExit.ExitApp();
                    break;
            }
        }
    }
    

}
